from idautils import *
from idaapi import * 
from json import *


ea = ScreenEA()
func_json = {"range":[]}
for funcea in Functions(SegStart(ea),SegEnd(ea)):
	functionName = GetFunctionName(funcea)
	functionStart = funcea
	functionEnd = (FindFuncEnd(funcea))

	func_json["range"].append({"name":functionName, "start":functionStart, "end":functionEnd})

filepath = "C:\\htrace_config.json"	# set path
fp = open(filepath,"wb")
fp.write(dumps(func_json, indent=4))
fp.close()

print dumps(func_json, indent=4)
print "[+] json file created.."