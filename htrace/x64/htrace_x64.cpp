﻿#include "pin.H"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <typeinfo>
#include <unistd.h>
#include <csignal>
#include <unordered_map>
#include <stdint.h>
#include <string.h>
#include <cstdlib>
#include <boost/regex.hpp>
#include <algorithm>
#include <sstream>
#include <list>
#include <iomanip>
#include <json/json.h>
#include "./util/string_method.h"

#define SSTR( x ) dynamic_cast< std::ostringstream &>( \
        ( std::ostringstream() << std::dec << x ) ).str()
#define SIGSEGV_FLAG 1
#define CONTEXT_FLAG 1
using namespace std;

list<string> tmp_inst_list;
list<int64_t> tmp_call_list;
list<string> output_map;
int call_count = 0;

list<string> output_ha;
list<int64_t> push_list;

CONTEXT snapshot;

string binary_name = "";
string output = "";
convert converter;

map<string, vector<int> > config_map;
bool config_flag = false;

string regs_order[] = {"RAX","RBX","RCX","RDX","RDI","RSI","RBP","RSP","RIP","R8","R9","R10","R11","R12","R13","R14","R15"};
string args_order[] = {"di","si","dx","cx","8","9"};


VOID INS_Process(string func_id, CONTEXT *ctx, UINT64 addr, ADDRINT callTargetAddr);
VOID show_register();
VOID Trace(TRACE trace, VOID *v);
string DisplayContext(CONTEXT *ctx, UINT32 flag);
BOOL signal_handler(THREADID tid, INT32 sig, CONTEXT *ctx, BOOL hasHandler, const EXCEPTION_INFO *pExceptInfo, VOID *v);
VOID Fini(INT32 code, VOID *v);
int64_t get_value(CONTEXT *ctx, string reg_name);
VOID Add_return(string instruction, CONTEXT *ctx, UINT64 memOp);
VOID Instruction(INS ins, VOID *v);
VOID INS_Push(string instruction);
VOID getRetFlag(CONTEXT *ctx, string instruction, ADDRINT refsfrom, ADDRINT addr);
int setConfig(string filename);


VOID getRetFlag(CONTEXT *ctx, string instruction, ADDRINT refsfrom, ADDRINT addr)
{
    string tmp_string = "";
    for(list<string>::iterator it=output_map.begin();it!=output_map.end();it++)
    {
        tmp_string = *it;
        if(tmp_string.find("[DEBUG :: from 0x" + converter.to_hex64(refsfrom))!=string::npos && tmp_string.find("= 0x")==string::npos)
        {
            *it = converter.str_replace(*it, " = unknown"," = 0x" + converter.to_hex64(get_value(ctx,"rax")));
            break;
        }
    }
}

VOID Instruction(INS ins, VOID *v)
{
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)getRetFlag,
        IARG_PTR, new string(INS_Disassemble(ins)),
        IARG_PTR, INS_Address(ins),
        IARG_PTR, ins,
        IARG_END);
}

VOID Add_return(string instruction, CONTEXT *ctx, UINT64 memOp)
{
    tmp_inst_list.clear();
    output += converter.str_replace(output, " = hh"," = 0x" + converter.to_hex64(get_value(ctx,"rax")));
    cout << output << endl;
}

VOID PUSH_Process(string instruction, UINT64 memOp, CONTEXT *ctx)
{
     boost::regex re2("push (r|e).*");
     stringstream ss;
     int64_t x = 0;
     // additional option ~ 
     //cout << instruction << endl;
     if(boost::regex_match(instruction,re2))
     {
          push_list.push_back(get_value(ctx, converter.str_replace(instruction,"push ","")));
     }

     else if(instruction.find("push 0x")!=string::npos)
     {  
        ss << hex << converter.str_replace(instruction,"push 0x","");
        ss >> x;
        push_list.push_back(x);
     }
}

VOID INS_Push(string instruction)
{ 
    tmp_inst_list.push_back(instruction);
    //cout << instruction << endl;
}


VOID INS_Process(string func_id, CONTEXT *ctx, ADDRINT refsAddr, ADDRINT callTargetAddr)
{
    if(callTargetAddr>0x400000 && callTargetAddr<0x500000)
    {
        string *params = new string[tmp_inst_list.size()];
        bool check = false;
        int i=0;
        int inst_list_count = tmp_inst_list.size();
        list<int64_t> argv_list;

        output += "[DEBUG :: from 0x" + converter.to_hex64(refsAddr) +"] sub_" + converter.to_hex64(callTargetAddr) + "(";
        if(config_flag == true)
        {
            for(map<string, vector<int> >::iterator it=config_map.begin();it!=config_map.end();it++)
            {
                if(config_map[it->first][0] == (int)callTargetAddr)
                {
                    output = "[DEBUG :: from 0x" + converter.to_hex64(refsAddr) +"] " + it->first + "(";
                    break;
                }
            }
        }

        for(i=0;i<inst_list_count;i++)
        {
            params[i] = tmp_inst_list.front();
            tmp_inst_list.pop_front();
        }

        int j=0;
        int k=0;
        string regular_expression = "";
        list<int> overlap_reg;

        bool mov_flag = false;
        bool reg_found = false;

        for(j=0;j<6;j++)
        {
            for(k=0;k<inst_list_count;k++)
            {
                // 정규식으로 검사 
                reg_found = (find(overlap_reg.begin(),overlap_reg.end(), j)==overlap_reg.end());
                if(reg_found == false)
                    continue;

                boost::regex re("(mov|lea|pop) ([re]|)" + args_order[j] + "(.|)(,|)(.|)*");
                //output += "\n" + params[k] + "\n";
                //output += SSTR(boost::regex_match(params[k],re));
                if(boost::regex_match(params[k],re))
                {  
                    output += "0x" + converter.to_hex64(get_value(ctx,"r" + args_order[j])) + ",";
                    overlap_reg.push_back(j);
                    argv_list.push_back(get_value(ctx,"r" + args_order[j]));
                    check = true;

                    if(j==5)
                    {
                        mov_flag = true;
                    }
                    continue;
                }

                else
                {
                    check = false;
                }
            }

            if(check==false)
                break;
        }

        if(mov_flag == true && push_list.size()!=0)
        {
            // push까지 처리
            for(list<int64_t>::reverse_iterator it4 = push_list.rbegin();it4!=push_list.rend();it4++)
            {
                output += "0x" + converter.to_hex64(*it4) + ",";
                argv_list.push_back(*it4);
            }
            mov_flag = false;
        }
        output += ") = unknown\n";
        output = converter.str_replace(output, ",)",")");
        int argc = 0;
        int64_t* tmp_addr = 0;
        tmp_addr = (int64_t*)0x400000;
        //cout << IMG_Valid(IMG_FindByAddress(0x61616161)) << endl;
        //int* a = (int*)0x400DF6;
        //cout << converter.to_hex64(*a) << endl;

        for(list<int64_t>::iterator it1=argv_list.begin();it1!=argv_list.end();it1++)
        {
            output += "argv[0x" + converter.to_hex64(argc) + "] : ";
            output += "0x" + converter.to_hex64(*it1);
            PIN_LockClient();
            if(((int64_t)0x7f0000000000 < (int64_t)*it1 && (int64_t)0x7fffffffffff > (int64_t)*it1))
            {
                tmp_addr = (int64_t*)*it1;
                output += " -> 0x" + converter.to_hex64(*it1);

                while((0x7f0000000000 < *tmp_addr && 0x7fffffffffff > *tmp_addr))
                {
                    tmp_addr = (int64_t*)*tmp_addr;
                    output += " -> 0x" + converter.to_hex64(*tmp_addr);
                }
                output += "(" + converter.reverse(converter.decode(converter.to_hex64(*tmp_addr).length()%2==1?"0"+converter.to_hex64(*tmp_addr):converter.to_hex64(*tmp_addr)))  + ")";
                
            }
            //output += "\n" + IMG_Name(IMG_FindByAddress(*it1));
            PIN_UnlockClient();
            output += "\n";
            argc++;
        }

        output_map.push_back(output);
        output = "";
        //cout << output << endl;    
        push_list.clear();
        tmp_inst_list.clear();
    }

    else
    {
        push_list.clear();
        tmp_inst_list.clear();
    }
}

int64_t get_value(CONTEXT *ctx, string reg_name)
{
    if(reg_name == "rax" || reg_name == "eax" || reg_name == "ax")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_RAX);

    else if(reg_name == "rbx" || reg_name == "ebx" || reg_name == "bx")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_RBX);

    else if(reg_name == "rcx" || reg_name == "ecx" || reg_name == "cx")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_RCX);

    else if(reg_name == "rdx" || reg_name == "edx" || reg_name == "dx") 
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_RDX);
    
    else if(reg_name == "rdi" || reg_name == "edi" || reg_name == "di")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_RDI);
    
    else if(reg_name == "rsi" || reg_name == "esi" || reg_name == "si")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_RSI);
    
    else if(reg_name == "rbp" || reg_name == "ebp" || reg_name == "bp")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_RBP);

    else if(reg_name == "rsp" || reg_name == "esp" || reg_name == "sp")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_RSP);

    else if(reg_name == "rip" || reg_name == "pc")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_RIP);

    else if(reg_name == "r8")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_R8);

    else if(reg_name == "r9")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_R9);
    
    else if(reg_name == "r10")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_R10);
    else if(reg_name == "r11")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_R11);
    else if(reg_name == "r12")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_R12);

    else if(reg_name == "r13")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_R13);

    else if(reg_name == "r14")
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_R14);
    else if(reg_name == "r15") 
        return PIN_GetContextReg(ctx, LEVEL_BASE::REG_R15);

    else
        return 0;
}

// IMG_Valid(IMG_FindByAddress(my_address))
VOID Trace(TRACE trace, VOID *v)
{   
    //cout << IMG_Valid(IMG_FindByAddress(0x61616161)) << endl;
    //int* a = (int*)0x400DF6;
    //cout << converter.to_hex64(*a) << endl;
    ostringstream vStream;
    INS tmp_ins;

    for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
    {
        if(1==1)//((INS_Address(BBL_InsHead(bbl))>0x400000 && INS_Address(BBL_InsHead(bbl))<0x500000))
        {
            for(list<int64_t>::iterator it=tmp_call_list.begin();it!=tmp_call_list.end();it++)
            {
                if(((signed)((signed)INS_Address(BBL_InsHead(bbl))-(signed)*it)<7) && ((signed)((signed)INS_Address(BBL_InsHead(bbl))-(signed)*it)>0))
                {
                    INS_InsertCall ( BBL_InsHead(bbl), IPOINT_BEFORE, (AFUNPTR)getRetFlag,
                        IARG_CONTEXT,
                        IARG_PTR, new string(INS_Disassemble(BBL_InsHead(bbl))),
                        IARG_PTR, *it,
                        IARG_INST_PTR,
                        IARG_END);
                }
            }

            //cout << "[0x" << converter.to_hex(INS_Address(BBL_InsHead(bbl))) << "] " << INS_Disassemble(BBL_InsHead(bbl)) << endl;
            if(!strstr(INS_Disassemble(BBL_InsTail(bbl)).c_str(), "call ") || strstr(INS_Disassemble(BBL_InsTail(bbl)).c_str(), "syscall"))
            {
                continue;
            }

            else
            {
                tmp_ins = BBL_InsHead(bbl);
                for(INS ins=tmp_ins;INS_Valid(ins);ins=INS_Next(ins))
                {
                    vStream.str("");
                    if(INS_IsCall(ins))
                    {
                        INS_InsertCall ( ins, IPOINT_TAKEN_BRANCH, (AFUNPTR)INS_Process,
                            IARG_PTR, new string(converter.str_replace(INS_Disassemble(ins), "call ","")),
                            IARG_CONTEXT,
                            IARG_INST_PTR,
                            IARG_BRANCH_TARGET_ADDR,
                            IARG_END);

                        tmp_call_list.push_back(INS_Address(ins));
                        tmp_inst_list.clear();
                        push_list.clear();
                    }

                    else if(((INS_Disassemble(ins).find("push ")!=string::npos)
                            && INS_Disassemble(INS_Next(ins)).compare("mov rbp, rsp")!=0)
                            || (INS_Disassemble(ins).find("push 0x")!=string::npos))
                    {
                        INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)PUSH_Process,
                            IARG_PTR, new string(INS_Disassemble(ins)),
                            IARG_PTR, INS_Address(ins),
                            IARG_CONTEXT,
                            IARG_END);
                    }

                    else
                    {

                        INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)INS_Push,
                            IARG_PTR, new string(INS_Disassemble(ins)),
                            IARG_END);
                    }
                }
            }
        }
    }
}

string DisplayContext(CONTEXT *ctx, UINT32 flag)
{
    Json::Value root;
    root["OS"] = "x64";
    root["state"] = (flag==CONTEXT_FLAG ? "CONTEXT" : "SIGSEGV");
    root["RAX"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_RAX)).c_str();
    root["RBX"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_RBX)).c_str();
    root["RCX"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_RCX)).c_str();
    root["RDX"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_RDX)).c_str();
    root["RDI"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_RDI)).c_str();
    root["RSI"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_RSI)).c_str();
    root["RBP"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_RBP)).c_str();
    root["RSP"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_RSP)).c_str();
    root["RIP"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_RIP)).c_str();
    root["R8"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_R8)).c_str();
    root["R9"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_R9)).c_str();
    root["R10"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_R10)).c_str();
    root["R11"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_R11)).c_str();
    root["R12"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_R12)).c_str();
    root["R13"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_R13)).c_str();
    root["R14"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_R14)).c_str();
    root["R15"] = converter.to_hex64(PIN_GetContextReg(ctx, LEVEL_BASE::REG_R15)).c_str();
    
    Json::StyledWriter writer;
    string strJSON = writer.write(root);

    return strJSON;
}

BOOL signal_handler(THREADID tid, INT32 sig, CONTEXT *ctx, BOOL hasHandler, const EXCEPTION_INFO *pExceptInfo, VOID *v)
{
     cout << "[DEBUG :: Notice] Segmentation Fault !! " << endl;
     cout << DisplayContext(ctx, SIGSEGV_FLAG) << endl;
     return true;
}

VOID Fini(INT32 code, VOID *v)
{
    for(list<string>::iterator it=output_map.begin();it!=output_map.end();it++)
    {
        cout << *it << endl;
    }
}

int setConfig(string filename)
{
    Json::Value root;
    Json::Reader reader;

    ifstream t;
    t.open(filename);

    if(!reader.parse(t, root))
        return -1;

    // parse symbol ~
    Json::Value range_child = root["range"];
    for(int i=0;i<(int)range_child.size();++i)
    {
        cout << "[DEBUG :: Notice] Symbol Loading..." << endl;
        cout << "[DEBUG :: Notice] name : " << range_child[i]["name"] << " start : 0x" << converter.to_hex64(range_child[i]["start"].asInt()) << " end : 0x" << converter.to_hex64(range_child[i]["end"].asInt()) << endl;
        config_map[range_child[i]["name"].asString()] = vector< int >{range_child[i]["start"].asInt(), range_child[i]["end"].asInt()};
    }

    config_flag = true;
    return 0;
}

int main(int argc,char **argv)
{
    PIN_Init(argc,argv);
    CODECACHE_FlushCache();

    int i=0;
    string tmp_string1 = "";
    string tmp_string2 = "";
    for(i=0;i<argc;i++)
    {
        tmp_string1 = argv[i];
        tmp_string2 = argv[i+1];

        if(tmp_string1.find("--config")!=string::npos && tmp_string2!="")
        {
            cout << "[DEBUG :: Notice] Thanks Config file..!" << endl;
            config_flag = true;
            if(setConfig(tmp_string2)==-1)
            {
                cout << "[DEBUG :: Notice] Config File Parse Error..." << endl;
                return 0;
            }

            break;
        }
    }

    PIN_InitSymbols();
    PIN_SetSyntaxIntel();
    PIN_InterceptSignal(SIGSEGV, signal_handler, 0);
    TRACE_AddInstrumentFunction(Trace,0);
    //INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini,0);
    PIN_StartProgram();
}