[ dependency library ]
http://jsoncpp.sourceforge.net - jsoncpp
http://www.boost.org/ - boost
https://software.intel.com/en-us/articles/pin-a-dynamic-binary-instrumentation-tool - pintool

[ compile options ]
g++ -DBIGARRAY_MULTIPLIER=1 -Wall -Werror -Wno-unknown-pragmas -Wno-unused-but-set-variable -fno-stack-protector -DTARGET_IA32E -DHOST_IA32E -fPIC -DTARGET_LINUX  -I../../../source/include/pin -I../../../source/include/pin/gen -I../../../extras/components/include -I../../../extras/xed-intel64/include -I../../../source/tools/InstLib -O3 -fomit-frame-pointer -fno-strict-aliasing   -c -std=c++11  -o obj-intel64/htrace_x64.o htrace_x64.cpp

g++ -shared -Wl,--hash-style=sysv -Wl,-Bsymbolic -Wl,--version-script=../../../source/include/pin/pintool.ver    -o obj-intel64/htrace_x64.so obj-intel64/htrace_x64.o  -L../../../intel64/lib -L../../../intel64/lib-ext -L../../../intel64/runtime/glibc -L../../../extras/xed-intel64/lib -I /usr/lib/boost/include -L /usr/lib/boost/lib -lpin -lxed -lpindwarf -ldl -ljson -lboost_regex

