#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdint.h>
class convert{
        public:
        std::string to_hex(int);
        std::string to_hex64(int64_t);
            std::string to_oct(int);
            std::string to_bin(int);
            std::string string_to_hex(string);
        std::string str_replace(std::string, std::string, std::string); 
        unsigned char val(char);
        std::string decode(std::string const&);
        std::string reverse(std::string);   
        protected:
        std::string int_to_bin(int); 
};

unsigned char convert::val(char c)
{
    if ('0' <= c && c <= '9') { return c      - '0'; }
    if ('a' <= c && c <= 'f') { return c + 10 - 'a'; }
    if ('A' <= c && c <= 'F') { return c + 10 - 'A'; }
    else { return -1; }
}

std::string convert::decode(std::string const& s)
{
    if ((s.size() % 2) != 0) { return "unknown"; }

    std::string result;
    result.reserve(s.size() / 2);

    for (std::size_t i = 0; i < s.size() / 2; ++i)
    {
        if(val(s[2 * i]) == -1 || val(s[2 * i + 1]) == -1)
            return "unknown";

        unsigned char n = val(s[2 * i]) * 16 + val(s[2 * i + 1]);
        result += n;
    }

    return result;
}

std::string string_to_hex(string input)
{
    static const char* const lut = "0123456789ABCDEF";
    size_t len = input.length();

    std::string output;
    output.reserve(2 * len);
    for (size_t i = 0; i < len; ++i)
    {
        const unsigned char c = input[i];
        output.push_back(lut[c >> 4]);
        output.push_back(lut[c & 15]);
    }
    return output;
}


std::string convert::to_hex64(int64_t argv)
{
    /*
        std::cout<<"0x" << std::hex << std::noshowbase << std::setw(2) << std::setfill('0') << 140737479975745 << std::endl;
        
        std::string result;
            std::stringstream ss;
            ss << std::hex <<to_convert;
            ss >> result;
            return result;
    */

    std::string result;
    std::stringstream ss;
    ss << std::hex << std::noshowbase << std::setw(2) << std::setfill('0') << argv;
    ss >> result;
    return result;
}

std::string convert::str_replace(std::string str, std::string pattern, std::string replace)
{
    std::string result = str;
    std::string::size_type pos = 0;
    std::string::size_type offset = 0;
    
    while((pos = result.find(pattern, offset))!=std::string::npos)
    {
        result.replace(result.begin() + pos, result.begin() + pos + pattern.size(), replace);  
            offset = pos + replace.size();  
    }
    return result;
}

std::string convert::int_to_bin(int number){
    static std::string result;
    static int level = 0;
    level++;
    if (number > 0){
        if (number % 2 == 0){
            result.append("0");
        }
        else{
            result.append("1");
        }
        int_to_bin(number / 2);
        level--;
    }
    if (level == 1) return reverse(result);
    return result;
}

std::string convert::reverse(std::string to_reverse){
    std::string result;
    for (int i = to_reverse.length()-1; i >=0 ; i--)
        result += to_reverse[i];
    return result;
}

std::string convert::to_hex(int to_convert){
    std::string result;
    std::stringstream ss;
    ss << std::hex <<to_convert;
    ss >> result;
    return result;
}

std::string convert::to_oct(int to_convert){
    std::string result;
    std::stringstream ss;
    ss << std::oct << to_convert;
    ss >> result;
    return result;
}

std::string convert::to_bin(int to_convert){
    return int_to_bin(to_convert);
}
